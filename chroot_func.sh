#!/bin/bash

RED="\033[31m"
GREEN="\033[32m"
YELLOW="\033[33m"
BLUE="\033[34m"
DEFAULT="\033[0m"

user()
(
    echo -ne "${YELLOW}Computer's name: ${DEFAULT}"
    read hostname
    echo "$hostname" > /etc/hostname

    echo -e "${YELLOW}Creating root password${DEFAULT}"
    passwd root

    while [ $? -ne 0 ]; do
        echo -e "${RED}Failed root password, please retry${DEFAULT}"
        passwd root
    done

    echo -e "${YELLOW}Let's  create a user${DEFAULT}"
    echo -ne "${YELLOW}enter the username:${DEFAULT} "
    read username

    echo -ne "${YELLOW}enter the complete name:${DEFAULT} "
    read globname

    useradd -m -g users -G wheel -c "$globname" -s /bin/bash "$username"

    echo -e "${YELLOW}Creating $globname password${DEFAULT}"
    passwd "$username"
    while [ $? -ne 0 ]; do
        echo -e "${RED}Failed $globname password, please retry${DEFAULT}"
        passwd "$username"
    done
)

locale()
(
    echo -e "LANG=en_US.UTF-8\nLC_ALL=fr_FR.UTF-8\nLC_COLLATE=C" > /etc/locale.conf
    echo -e "KEYMAP=fr\nFONT=eurlatgr" > /etc/vconsole.conf

    sed -i 's/#fr_FR.UTF-8/fr_FR.UTF-8/g' /etc/locale.gen
    sed -i 's/#en_US.UTF-8/en_US.UTF-8/g' /etc/locale.gen

    locale-gen > /dev/null

    export LANG=en_US.UTF-8
)

yay_install()
(
    cd /tmp
    su "$username" -c "git clone https://aur.archlinux.org/yay.git; cd yay; makepkg"
    cd yay
    pacman -U --noconfirm *.tar.xz
    cd ../..
)

aur()
(
    cd /tmp
    su $username -c "mkdir yay_tmp"
    cd yay_tmp
    su "$username" -c "yay -G dmenu2 nerd-fonts-terminus"
    for d in $(ls .); do
        cd "$d"
        su "$username" -c "makepkg"
        pacman -U --noconfirm *.tar.xz
        cd ..
    done
    cd ..
)

init_script()
(
    echo "exec i3" > /home/"$username"/.xinitrc
    chown "$username" /home/"$username"/.xinitrc

    echo 'setxkbmap fr' >> /usr/share/sddm/scripts/Xsetup

    echo -e 'xset-b\nstartx' >> /home/"$username"/.bash_profile

    sed -i 's/#\ %wheel\ ALL=(ALL)\ ALL/%wheel\ ALL=(ALL)\ ALL/g' /etc/sudoers
)
