######################################
#    Auto Archlinux Installer V 0.1  #
######################################

#!/bin/sh

timedatectl set-ntp true

ping -c 2 archlinux.org &> /dev/null

if [ $? -ne 0 ]; then
    echo 'Need to be connected to Internet to continue'
    exit 1
fi

echo 'Partitionning the disk'

ram_size=$(./ram_size.sh)
disk_size=$(./disk_size.sh)
root_size=$(($disk_size / 4))
home_size=$(($disk_size - $root_size))

offset_ram=$(($ram_size + 104857600))
offset_root=$(($offset_ram + $root_size))

wipefs -af /dev/sda

parted /dev/sda mklabel msdos

fdisk /dev/sda <<< "n
p
1
2048
+100M
a
n
p
2

+${ram_size}K
t
2
82
n
p
3

+${root_size}G
n
p


w
"
fdisk -l

mkfs.ext4 /dev/sda1

mkswap /dev/sda2
swapon /dev/sda2

mkfs.ext4 /dev/sda4
mkfs.ext4 /dev/sda3

mount /dev/sda3 /mnt

mkdir /mnt/{boot,home}

mount /dev/sda1 /mnt/boot
mount /dev/sda4 /mnt/home

echo 'Here is the partitions created:'

fdisk -l

## Selection of the 3 best mirrors depending on speed

echo -e '\nFiltering mirrors'

pacman -Sy --noconfirm pacman-contrib

cp /etc/pacman.d/mirrorlist /etc/pacman.d/mirrorlist.backup
awk '/^France$/{f=1}f==0{next}/^$/{exit}{print substr($0, 2)}' /etc/pacman.d/mirrorlist.backup
rankmirrors -n 3 /etc/pacman.d/mirrorlist.backup > /etc/pacman.d/mirrorlist

echo 'Downloading basic package needed by installation'

pacstrap /mnt base vim base-devel grub os-prober

genfstab -U /mnt >> /mnt/etc/fstab

cp chroot.sh /mnt
cp chroot.cmd /mnt
cp chroot_func.sh /mnt
chmod 777 /mnt/chroot.sh
chmod 777 /mnt/chroot_func.sh
arch-chroot /mnt ./chroot.sh
