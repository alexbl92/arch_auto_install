#!/bin/bash

source chroot_func.sh

if [ $? -ne 0 ]; then
    echo "\033[31mChroot File Missing\033[0m"
    exit 1
fi

user

while read line
do
    action="$(echo "$line" | cut -c 2-)"
    if [ "$line" == "" ]; then
        continue
    elif [ "$(echo "$line" | cut -c 1)" == '#' ]; then
        echo -ne "${GREEN}${action}${DEFAULT}:"
    else
        bash -c "$line"
        if [ $? -eq 0 ]; then
            echo -e "${YELLOW}Success${DEFAULT}"
        else
            echo -e "${RED}Fail${DEFAULT}"
        fi
    fi
done < chroot.cmd
