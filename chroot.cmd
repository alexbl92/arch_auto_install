#Setting timezone to Paris
ln -sf /usr/share/zoneinfo/Europe/Paris /etc/localtime &> /dev/null

#Setting PC time to UTC
hwclock --systohc --utc &> /dev/null

#Generating Locales
locale &> /dev/null

#Mkinitcpio
mkinitcpio -p linux &> /dev/null

#installing packages
pacman -Syu --noconfirm go git xorg-xinit xorg-server i3-wm i3status i3blocks i3lock \
vim rxvt-unicode networkmanager network-manager-applet git xorg-xset sddm \
xorg-fonts-misc chromium zip unzip bzip2 p7zip \
xorg-xset xorg-xev xf86-input-libinput scrot xorg-xkill xfce4-power-manager tree \
htop tig tar xorg-xrandr &> /dev/null

#Installing grub on disk
grub-install --no-floppy --recheck /dev/sda &> /dev/null

#Configuring grub
grub-mkconfig -o /boot/grub/grub.cfg &> /dev/null

#Installing Yay
yay_install &> /dev/null

#Installing aur packages with Yay
aur &> /dev/null

#Enabling sddm
systemctl enable sddm &> /dev/null

#Enabling Netword Manager
systemctl enable NetworkManager &> /dev/null

#Setting up startup script
init_script &> /dev/null

echo 'arch-chroot over'

exit 0
